package pl.marekserwanski.webfluxshooter.domain.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static pl.marekserwanski.webfluxshooter.domain.user.TestUsersFactory.defaultUser;
import static pl.marekserwanski.webfluxshooter.domain.user.UserDaoAuthenticator.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService service;

    @Test
    public void shouldLoadByUserName() {
        when(userRepository.findByUsername(DEFAULT_USERNAME)).thenReturn(Optional.of(defaultUser()));

        UserDetails result = service.loadUserByUsername(DEFAULT_USERNAME);

        assertThat(result.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(result.getPassword()).isEqualTo(DEFAULT_PASS);
    }

}
