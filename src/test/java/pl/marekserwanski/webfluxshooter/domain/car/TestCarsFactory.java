package pl.marekserwanski.webfluxshooter.domain.car;

class TestCarsFactory {
    static final String DUMMY_BRAND = "syrena";

    static Car defaultCar() {
        return new Car(DUMMY_BRAND);
    }
}
