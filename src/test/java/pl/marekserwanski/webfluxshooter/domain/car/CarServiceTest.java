package pl.marekserwanski.webfluxshooter.domain.car;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.marekserwanski.webfluxshooter.domain.car.CarConfig.DEFAULT_CAR_BRAND;
import static pl.marekserwanski.webfluxshooter.domain.car.TestCarsFactory.defaultCar;
import static pl.marekserwanski.webfluxshooter.domain.car.TestCarsFactory.DUMMY_BRAND;

@ExtendWith(MockitoExtension.class)
public class CarServiceTest {

    @Mock
    CarRepository carRepository;

    @InjectMocks
    CarService carService;

//    @Test
//    public void shouldGetFirstCar() {
//        Car car = defaultCar();
//        when(carRepository.findByBrand(DEFAULT_CAR_BRAND)).thenReturn(Optional.of(car));
//
//        CarDto result = carService.getFirstCar();
//
//        assertThat(result).isNotNull();
//        assertThat(result.getBrand()).isEqualTo(car.getBrand());
//    }

    @Test
    public void shouldNotFoundCar() {
//        assertThrows(CarNotFoundException.class, () -> carService.getFirstCar());
    }

//    @Test
//    public void shouldUdateFirstCar() {
//        Car car = defaultCar();
//        when(carRepository.findByBrand(DEFAULT_CAR_BRAND)).thenReturn(Optional.of(car));
//
//        carService.updateFirstCar();
//
//        assertThat(car.getLastUpdateTime()).isNotNull();
//    }

    @Test
    public void shouldAddNewCar() {
        carService.addNewCar(DUMMY_BRAND);
        verify(carRepository).save(any(Car.class));
    }

//    @Test
//    public void shouldNotAddNewCar() {
//        when(carRepository.findByBrand(DUMMY_BRAND)).thenReturn(Optional.of(defaultCar()));
//        assertThrows(CarAlreadyExistException.class, () -> carService.addNewCar(DUMMY_BRAND));
//    }
}
