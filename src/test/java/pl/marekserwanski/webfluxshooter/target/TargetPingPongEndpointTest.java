package pl.marekserwanski.webfluxshooter.target;

import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**Sample mvc test.
 * Creating context in @ExtendWith(SpringExtension.class) is fragile, so here is simple unit-purpose MockMvc build */
public class TargetPingPongEndpointTest {

    MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new TargetPingPongEndpoint()).build();

    @Test
    public void makeGetPingPong() throws Exception {

        mockMvc.perform(post("/pingpong/open"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.backendTotal").isNotEmpty());

    }
}
