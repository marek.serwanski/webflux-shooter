package pl.marekserwanski.webfluxshooter;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@EnableReactiveMongoRepositories
public class MongoConfig extends AbstractReactiveMongoConfiguration {

    private static final String DB_NAME = "test";
    private static final String CONN_STRING = "mongodb://localhost:27015/" + DB_NAME;

    @Override
    protected String getDatabaseName() {
        return DB_NAME;
    }

    @Bean
    public MongoClient reactiveMongoClient() {
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(CONN_STRING))
                .build();

        return MongoClients.create(mongoClientSettings);
    }
}
