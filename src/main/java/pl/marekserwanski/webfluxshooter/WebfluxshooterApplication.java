package pl.marekserwanski.webfluxshooter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebfluxshooterApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebfluxshooterApplication.class, args);
	}

}
