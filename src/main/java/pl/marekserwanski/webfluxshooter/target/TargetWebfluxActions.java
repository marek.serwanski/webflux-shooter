package pl.marekserwanski.webfluxshooter.target;

import org.springframework.stereotype.Component;
import pl.marekserwanski.webfluxshooter.domain.car.CarDto;
import pl.marekserwanski.webfluxshooter.domain.car.CarService;
import pl.marekserwanski.webfluxshooter.utils.Timer;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Component
public class TargetWebfluxActions {

    private static final int DEFAULT_ACTIONS_COUNT = 100;
    private static final int MAX_ACTIONS_COUNT = 200;
    private static final int DEFAULT_MULTIPLE_CRUD_CYCLES = 100;

    private final CarService carService;

    TargetWebfluxActions(CarService carService) {
        this.carService = carService;
    }

    Flux<CarDto> getFluxCarsStream(Integer counter, Timer timer) {
        return carService.getFluxCars(validaterCounter(counter), timer);
    }

    private int validaterCounter(Integer counter) {
        if (counter == null || counter < 0 || counter > MAX_ACTIONS_COUNT) {
            return DEFAULT_ACTIONS_COUNT;
        }
        return counter;
    }

    CarDto makeSimpleCRUDactions() {
        return simpleCRUDactions();
    }

    void makeMultipleCRUDactions() {
        for (int i=0 ; i<DEFAULT_MULTIPLE_CRUD_CYCLES ; i++) {
            simpleCRUDactions();
        }
    }

    //does not make business sense, let's check what is total time of all CRUD actions
    private CarDto simpleCRUDactions() {
        String dummyCar = UUID.randomUUID().toString();

        carService.addNewCar(dummyCar);                   //C
        carService.getMonoCar(dummyCar).block();          //R
        carService.updateFirstCar();                      //U
        carService.deleteCar(dummyCar);                   //D

        return null;
    }

}
