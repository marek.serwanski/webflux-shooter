package pl.marekserwanski.webfluxshooter.target;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.marekserwanski.webfluxshooter.utils.Timer;
import pl.marekserwanski.webfluxshooter.utils.TimingsRequest;
import pl.marekserwanski.webfluxshooter.utils.TimingsResponse;

import static pl.marekserwanski.webfluxshooter.utils.Timer.start;
import static pl.marekserwanski.webfluxshooter.utils.TimingsResponse.buildSimpleResponse;

@RestController
@RequestMapping("/pingpong/open")
public class TargetPingPongEndpoint {

    @GetMapping
    public TimingsResponse makeGetPingPong(Long frontendStartTime) {
        Timer timer = start();
        return buildSimpleResponse(frontendStartTime, timer.getProccessTimeInMS());
    }

    @PostMapping
    TimingsResponse makePostPingPong(TimingsRequest timingsRequest) {
        Timer timer = start();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }
}
