package pl.marekserwanski.webfluxshooter.target;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.marekserwanski.webfluxshooter.domain.car.CarDto;
import pl.marekserwanski.webfluxshooter.utils.Timer;
import pl.marekserwanski.webfluxshooter.utils.TimingsRequest;
import pl.marekserwanski.webfluxshooter.utils.TimingsResponse;
import reactor.core.publisher.Flux;

import static pl.marekserwanski.webfluxshooter.utils.Timer.start;
import static pl.marekserwanski.webfluxshooter.utils.TimingsResponse.buildSimpleResponse;

@RestController
@RequestMapping("/flux")
public class TargetWebfluxEndpoint {

    private final TargetWebfluxActions actions;

    TargetWebfluxEndpoint(TargetWebfluxActions targetMongoActions) {
        this.actions = targetMongoActions;
    }

    @PostMapping("read")
    public Flux<CarDto> readFluxData(@RequestBody TimingsRequest timingsRequest) {
        Timer timer = start();
        return actions.getFluxCarsStream(timingsRequest.getActionsCount(), timer);
    }

    //CRUD endpoints obviously does not make much more sense in flux world.
    //Here are only for testing performance issues. It works with block() methods, so it ordinary non-reactive pattern.

    @PostMapping("crud/open")
    public TimingsResponse makeFluxCRUDActions(@RequestBody TimingsRequest timingsRequest) {
        Timer timer = start();
        actions.makeSimpleCRUDactions();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }

    @PostMapping("crud/complex/open")
    public TimingsResponse makeFluxCRUDComplexActions(@RequestBody TimingsRequest timingsRequest) {
        Timer timer = start();
        actions.makeMultipleCRUDactions();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }
}
