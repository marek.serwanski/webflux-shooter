package pl.marekserwanski.webfluxshooter.utils;

public class TimingsRequest {

    private Long frontendStartTime;
    private Integer actionsCount;

    public Long getFrontendStartTime() {
        return frontendStartTime;
    }

    public void setFrontendStartTime(Long frontendStartTime) {
        this.frontendStartTime = frontendStartTime;
    }

    public Integer getActionsCount() {
        return actionsCount;
    }

    public void setActionsCount(Integer actionsCount) {
        this.actionsCount = actionsCount;
    }
}
