package pl.marekserwanski.webfluxshooter.utils;

public class TimingsResponse {

    private final Long frontendStartTime;
    private final Long backendTotal;

    protected TimingsResponse(Long frontendStartTime, Long backendTotal) {
        this.frontendStartTime = frontendStartTime;
        this.backendTotal = backendTotal;
    }

    public static TimingsResponse buildSimpleResponse(Long frontendStartTime, Long backendTotal) {
        return new TimingsResponse(frontendStartTime, backendTotal);
    }

    public Long getBackendTotal() {
        return backendTotal;
    }

    public Long getFrontendStartTime() {
        return frontendStartTime;
    }
}
