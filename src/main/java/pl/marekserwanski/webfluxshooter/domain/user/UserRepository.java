package pl.marekserwanski.webfluxshooter.domain.user;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import java.util.Optional;

interface UserRepository extends ReactiveMongoRepository<User, Long> {

    Optional<User> findByUsername(String username);
}
