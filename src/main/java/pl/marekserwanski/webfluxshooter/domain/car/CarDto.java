package pl.marekserwanski.webfluxshooter.domain.car;

public class CarDto {

    private final String brand;
    private final Long backendTotal;

    public CarDto(String brand, Long proccessTime) {
        this.brand = brand;
        this.backendTotal = proccessTime;
    }

    public CarDto(String brand) {
        this.brand = brand;
        this.backendTotal = -1L;
    }

    public String getBrand() {
        return brand;
    }

    public Long getBackendTotal() {
        return backendTotal;
    }
}
