package pl.marekserwanski.webfluxshooter.domain.car;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.io.Serializable;
import java.util.Date;

import static java.time.Instant.now;
import static java.util.Date.from;

@Document
class Car implements Serializable {

    @MongoId
    private ObjectId id;
    private String brand;
    private CarVin VIN;
    private Date creationTime;
    private Date lastUpdateTime;

    Car() {
    }

    Car(String brand) {
        this.brand = brand;
        this.creationTime = from(now());
    }

    Car(String brand, CarVin VIN) {
        this(brand);
        this.VIN = VIN;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    String getBrand() {
        return brand;
    }

    Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    Date getCreationTime() {
        return creationTime;
    }

    void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    ObjectId getId() {
        return id;
    }

    void setId(ObjectId id) {
        this.id = id;
    }

    public CarVin getVIN() {
        return VIN;
    }

    public void setVIN(CarVin VIN) {
        this.VIN = VIN;
    }
}
