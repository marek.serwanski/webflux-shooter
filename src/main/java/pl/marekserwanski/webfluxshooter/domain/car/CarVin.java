package pl.marekserwanski.webfluxshooter.domain.car;

import static java.util.UUID.randomUUID;

//mongo repository does not like private constructors. But likes setters/getters :/ no nice pattern can be used
class CarVin {

    private String VIN;

    CarVin() {
        VIN = randomUUID().toString();
    }

    CarVin(String vin) {
        VIN = vin;
    }

    static CarVin RANDOM_VIN() {
        return new CarVin();
    }

    String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }
}
