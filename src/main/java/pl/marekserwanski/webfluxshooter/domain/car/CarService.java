package pl.marekserwanski.webfluxshooter.domain.car;

import pl.marekserwanski.webfluxshooter.utils.Timer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static java.lang.String.format;
import static java.time.Instant.now;
import static java.util.Date.from;
import static org.springframework.data.domain.Pageable.ofSize;
import static pl.marekserwanski.webfluxshooter.domain.car.CarConfig.DEFAULT_CAR_BRAND;
import static pl.marekserwanski.webfluxshooter.domain.car.CarVin.RANDOM_VIN;

public class CarService {

    private final CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public Flux<CarDto> getFluxCars(int counter, Timer timer) {
        return carRepository.findByBrand(DEFAULT_CAR_BRAND, ofSize(counter))
                .map(car -> new CarDto(car.getBrand(), timer.getProccessTimeInMS()));
    }

    public Mono<CarDto> getMonoCar(String brand) {
        return carRepository.findByBrand(brand).mapNotNull(car -> new CarDto(car.getBrand()));
    }

    public void updateFirstCar() {
        carRepository.findFirstByBrand(DEFAULT_CAR_BRAND).blockOptional().ifPresentOrElse(
                car -> {
                car.setLastUpdateTime(from(now()));
                carRepository.save(car).block();
            }, () -> {
                throw new CarNotFoundException(format("Car %s not found", DEFAULT_CAR_BRAND));
            });
    }

    public void addNewCar(String brand) {
        carRepository.save(new Car(brand, RANDOM_VIN())).block();
    }

    public void deleteCar(String brand) {
        carRepository.findFirstByBrand(brand).blockOptional().ifPresent(car -> carRepository.delete(car).block());
    }
}
