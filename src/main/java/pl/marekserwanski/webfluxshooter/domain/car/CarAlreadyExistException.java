package pl.marekserwanski.webfluxshooter.domain.car;

class CarAlreadyExistException extends RuntimeException {

    CarAlreadyExistException(String brand) {
        super ("Car already exists: " + brand);
    }
}
