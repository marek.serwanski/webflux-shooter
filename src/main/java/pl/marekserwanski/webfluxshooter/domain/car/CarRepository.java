package pl.marekserwanski.webfluxshooter.domain.car;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

interface CarRepository extends ReactiveMongoRepository<Car, Long> {

    Flux<Car> findByBrand(String brand, Pageable pageable);

    Mono<Car> findByBrand(String brand);

    Mono<Car> findFirstByBrand(String brand);
}
