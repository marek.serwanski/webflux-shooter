package pl.marekserwanski.webfluxshooter.domain.car;

class CarNotFoundException extends RuntimeException {

    CarNotFoundException(String msg) {
        super (msg);
    }
}
