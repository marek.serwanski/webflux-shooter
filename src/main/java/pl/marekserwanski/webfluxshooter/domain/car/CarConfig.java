package pl.marekserwanski.webfluxshooter.domain.car;

import org.slf4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;
import static pl.marekserwanski.webfluxshooter.domain.car.CarVin.RANDOM_VIN;

@Configuration
class CarConfig {

    private static final Logger log = getLogger(CarConfig.class);
    static final String DEFAULT_CAR_BRAND = "Syrena";
    private static final int MAX_CARS_IN_DB = 200;
    private static final int OPTIMUM_CARS_IN_DB = 100;

    @Bean
    CarService carService(CarRepository carRepository) {
        initTestCars(carRepository);
        return new CarService(carRepository);
    }

    private void initTestCars(CarRepository carRepository) {
        carRepository.findAll().count().blockOptional().ifPresentOrElse(carsNumber -> {
            if (carsNumber >= MAX_CARS_IN_DB) {
                log.info("DB has already enough cars for testing");
            } else {
                carRepository.saveAll(Flux.fromIterable(newCarsList())).subscribe();
            }
        },() -> {
            throw new CarNotFoundException("Cannot count any cars from DB - unsuspected problem.");
        });
    }

    private List<Car> newCarsList() {
        List<Car> result = new ArrayList<>();
        for (int i=0 ; i<OPTIMUM_CARS_IN_DB ; i++) {
            result.add(new Car(DEFAULT_CAR_BRAND, RANDOM_VIN()));
        }
        return result;
    }
}
